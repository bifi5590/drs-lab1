package drs.l1;

import drs.l1.gui.ScheduleFrame;
import drs.l1.scheduler.algorithms.RateMonotonic;
import drs.l1.scheduler.Scheduler;
import drs.l1.scheduler.Task;

/**
 *
 * @author Alexander von Birgelen
 */
public class L1VonBirgelen {
    
    //This is the amount of Task sets. Increase this number when adding more
    //test cases. The test cases are implemented in the switch-case statement 
    //below.
    private static final int TASK_SET_COUNT = 10;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //The program performs 10 Test cases:
        for(int set=0; set<TASK_SET_COUNT;set++) {
            
            //Create a new Schedule. The specific scheduling Algorithm is a 
            //Sub-Class of Scheduler.
            Scheduler s = new RateMonotonic();

            //Select the Test case:
            //The first argument of the Constructor of Task is the Period,
            //the second parameter is the computation time.
            switch(set) {
                case 0:
                    //From lecture
                    s.addTask(new Task(4,1));
                    s.addTask(new Task(6,4));
                    break;
                case 1:
                    //From lecture
                    s.addTask(new Task(4,1));
                    s.addTask(new Task(7,5));
                    break;
                case 2:
                    //From lecture
                    s.addTask(new Task(5,2));
                    s.addTask(new Task(6,3));
                    break;
                case 3:
                    //From lecture (deadline miss)
                    s.addTask(new Task(5,2));
                    s.addTask(new Task(6,3));
                    s.addTask(new Task(10,1));
                    break;
                case 4:
                    //From lecture
                    s.addTask(new Task(60,20));
                    s.addTask(new Task(30,10));
                    s.addTask(new Task(15,5));
                    break;
                case 5:
                    //5 Tasks
                    s.addTask(new Task(5,1));
                    s.addTask(new Task(6,2));
                    s.addTask(new Task(10,2));
                    s.addTask(new Task(20,2));
                    s.addTask(new Task(10,1));
                    break;
                case 6:
                    //3 identical Tasks
                    s.addTask(new Task(15,5));
                    s.addTask(new Task(15,5));
                    s.addTask(new Task(15,5));
                    break;
                case 7:
                    //4 Tasks.
                    s.addTask(new Task(5,1));
                    s.addTask(new Task(9,3));
                    s.addTask(new Task(30,1));
                    s.addTask(new Task(30,11));
                    break;
                case 8:
                    //Sample
                    s.addTask(new Task(4,1));
                    s.addTask(new Task(8,4));
                    s.addTask(new Task(16,3));
                    break;
                case 9: 
                    //Not schedulable using rm. but can be done with edf
                    s.addTask(new Task(5,2));
                    s.addTask(new Task(7,4));
                    break;
                default:
                    System.out.println("Wrong Task set selection.");
                    return;
            }

            //Calculate the schedule:
            s.createSchedule();
            
            //Print the calculated schedule:
            s.printSchedule(true);

            //To visualize the calcuated schedule a GUI is needed:
            ScheduleFrame gui = new ScheduleFrame(s);
            //Show gui:
            gui.setVisible(true);
            //The gui can save the Plot as a png file by calling this method:
            gui.save("TaskSet-"+set);

        }
        
    }
    
}
