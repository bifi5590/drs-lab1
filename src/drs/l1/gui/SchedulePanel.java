package drs.l1.gui;

import drs.l1.scheduler.Scheduler;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author Alexander von Birgelen
 */
public class SchedulePanel extends JPanel {
    
    //The scheduler to use:
    private Scheduler s;
    
    /**
     * Set the schedule to use for plotting:
     * @param s the schedule to use.
     */
    public void setSchedule(Scheduler s) {
        this.s = s;
        //Try to force repaint after setting schedule:
        this.invalidate();
        this.repaint();
    }
    
    /**
     * Paint the schedule.
     * It is best to look at a plot first and then look at the source. It is
     * easier to understand the placing of the different elements in the plot 
     * then.
     * @param g 
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        //Only paint when a schedule was set:
        if(s == null) {
            return;
        }
        
        //Some static values for placing the drawn elements:
        int leftspace = 100;
        int infospace = 65;
        int textspace = 10;
        
        //Get the height values to allow dynamic painting when resizing
        int height = this.getHeight() - 3*textspace;
        int width = this.getWidth();
        
        //HeightPerTask is the amount of pixels for one Task in the y direction.
        int HeightPerTask = (int) ((height) / (s.getTasks().size()));
        //WidthPerSlot is the amount of pixels for one time slot in x direction.
        int WidthPerSlot = (width-leftspace) / s.getLCMPeriod();
        
        //Print information:
        g.setColor(Color.black);
        g.drawString(s.toString(), textspace, 2*textspace);
        
        //Draw color information:
        g.setColor(Color.green);
        g.fillRect(4*(infospace), textspace, textspace, textspace);
        g.setColor(Color.black);
        g.drawString("Work", 4*(infospace)+textspace+textspace/2, 2*textspace);
        
        g.setColor(Color.red);
        g.fillRect(5*(infospace), textspace, textspace, textspace);
        g.setColor(Color.black);
        g.drawString("Miss", 5*(infospace)+textspace+textspace/2, 2*textspace);
        
        g.setColor(Color.blue);
        g.fillRect(6*(infospace), textspace, 2, textspace);
        g.setColor(Color.black);
        g.drawString("Period", 6*(infospace)+textspace/2, 2*textspace);
        
        //Paint Slot Grid:
        for(int i=0;i<=s.getLCMPeriod();i++) {
            g.setColor(Color.LIGHT_GRAY);
            g.drawLine(leftspace + i*WidthPerSlot, 3*textspace, leftspace + i*WidthPerSlot, height + 3*textspace);
        }
        
        //Paint every Task:
        for(int i=0; i<s.getTasks().size();i++) {
            
            //Paint Name:
            g.setColor(Color.black);
            g.drawString(s.getTasks().get(i).toString(), textspace, i*HeightPerTask+HeightPerTask/2 + 3*textspace);

            //Paint Slots:
            for(int j=0; j<s.getLCMPeriod()+1;j++) {
                
                if(s.getSchedule()[i][j] > 0) {
                    g.setColor(Color.green);
                    g.fillRect(leftspace + j*WidthPerSlot, i*HeightPerTask + 3*textspace, WidthPerSlot, HeightPerTask);
                }else if(s.getSchedule()[i][j] < 0) {
                    g.setColor(Color.red);
                    g.fillRect(leftspace + j*WidthPerSlot, i*HeightPerTask + 3*textspace, WidthPerSlot, HeightPerTask);
                }
                
            }
            
            //Paint Periods:
            for(int j=0; j<=s.getLCMPeriod();j+=s.getTasks().get(i).getT()) {
                g.setColor(Color.blue);
                g.drawLine(leftspace + j*WidthPerSlot, i*HeightPerTask + 3*textspace, leftspace + j*WidthPerSlot, i*HeightPerTask + HeightPerTask + 3*textspace);
            }
            
        }
        
        
    }
    
}
